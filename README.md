# How to install
install docker on your pc and run these command
- cp .env.example .env
- composer install
- npm install
- php artisan key:generate
- php artisan migrate:fresh --seed

you might have to run in seperate command windows these lines below because the output message
- php artisan serve
- npm run watch

and open browser with this url
http://localhost:3000/

user: admin@gmail.com
password: password