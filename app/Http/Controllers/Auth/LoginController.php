<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\SocialAccount;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo()
    {
        session()->flash('success', 'You are logged in!');
        return $this->redirectTo;
    }

    //BEGIN SOCIALITE
    public function redirectToProvider($provider = 'facebook')
    {
        return Socialite::driver($provider)->redirect();
    }
    public function handleProviderCallback($provider = 'facebook')
    {
        $providerUser = Socialite::driver($provider)->user();
        $user = $this->createOrGetUser($provider, $providerUser);
        auth()->login($user);
        return redirect()->to('/home');
    }
    public function createOrGetUser($provider, $providerUser)
    {
        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        } else {
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {

                $imageName = $provider . "_" . $providerUser->getId() . ".png";
                $url = $providerUser->avatar_original . "&access_token={$providerUser->token}";
                $contents = file_get_contents($url);
                Storage::disk('public')->put("/users/{$imageName}", $contents);

                $name = $providerUser->getName();
                list($first, $last) = explode(' ', "$name ");

                $user = User::create([
                    'name' => $name,
                    'first_name' => $first,
                    'last_name' => $last,
                    'email' => $providerUser->getEmail(),
                    'password' => bcrypt(rand(1000, 9999)),
                    'avatar' => $imageName,
                    'approved_at' => null,
                ]);
            }
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook',
            ]);
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }
    //END SOCIALITE
}
