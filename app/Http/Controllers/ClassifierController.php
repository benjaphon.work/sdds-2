<?php

namespace App\Http\Controllers;

use App\Classifier;
use Illuminate\Http\Request;

class ClassifierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Classifier  $Classifier
     * @return \Illuminate\Http\Response
     */
    public function show(classifier $classifier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Classifier  $Classifier
     * @return \Illuminate\Http\Response
     */
    public function edit(Classifier $classifier)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\classifier  $classifier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classifier $classifier)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\classifier  $classifier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classifier $classifier)
    {
        //
    }

    function list($table, $field) {
        return Classifier::where('table', $table)->where('field', $field)->get();
    }
}
