<?php

namespace App\Http\Controllers;

use App\Item;
use App\ItemMeta;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $validated = $request->validate([
            'name' => 'required|unique:items,name|max:255',
            'image' => 'nullable|sometimes|image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);

        $model = new Item;

        $model->name = $request['name'];
        $model->description = $request['description'] ? $request['description'] : '';
        $model->price_rate = $request['price_rate'] ? $request['price_rate'] : 0;
        $model->amount = $request['amount'] ? $request['amount'] : 0;
        $model->user_id = auth()->user()->id;

        if ($request->file('image')) {
            $model->image_path = basename($request->file('image')->store('items', 'public'));
        }

        $model->timestamps = true;
        $model->touch();
        if ($model->save()) {

            $request['dataTags'] = json_decode($request['dataTags']);
            foreach ($request['dataTags'] as $tag) {
                Tag::updateOrCreate(
                    ['name' => $tag->name]
                );
            }

            $request['tags'] = json_decode($request['tags']);
            $model->tags()->detach();
            foreach ($request['tags'] as $tag) {
                $model->tags()->attach($tag->name);
            }

            $request['items'] = json_decode($request['items']);
            $model->items()->detach();
            foreach ($request['items'] as $item) {
                $model->items()->attach($item->id);
            }

            $request['metas'] = json_decode($request['metas']);
            $model->metas()->delete();
            foreach ($request['metas'] as $meta) {
                if ($meta->key && $meta->value) {
                    $metaModel = new ItemMeta(['key' => $meta->key, 'value' => $meta->value]);
                    $model->metas()->save($metaModel);
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        return view('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        // dd($request->all());

        $model = $item;

        $validated = $request->validate([
            'name' => "required|unique:items,name,{$model->id}|max:255",
            'image' => 'nullable|sometimes|image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);

        $model->name = $request['name'];
        $model->description = $request['description'] ? $request['description'] : '';
        $model->price_rate = $request['price_rate'] ? $request['price_rate'] : 0;
        $model->amount = $request['amount'] ? $request['amount'] : 0;

        if ($request->file('image')) {
            Storage::delete("/public/items/" . $model->image_path);
            $model->image_path = basename($request->file('image')->store('items', 'public'));
        }

        if ($request['reset_image'] === 'true') {
            Storage::delete("/public/items/" . $model->image_path);
            $model->image_path = null;
        }

        $model->timestamps = true;
        $model->touch();
        if ($model->save()) {

            $request['dataTags'] = json_decode($request['dataTags']);
            foreach ($request['dataTags'] as $tag) {
                Tag::updateOrCreate(
                    ['name' => $tag->name]
                );
            }

            $request['tags'] = json_decode($request['tags']);
            $model->tags()->detach();
            foreach ($request['tags'] as $tag) {
                $model->tags()->attach($tag->name);
            }

            $request['items'] = json_decode($request['items']);
            $model->items()->detach();
            foreach ($request['items'] as $item) {
                $model->items()->attach($item->id);
            }

            $request['metas'] = json_decode($request['metas']);
            $model->metas()->delete();
            foreach ($request['metas'] as $meta) {
                if ($meta->key && $meta->value) {
                    $metaModel = new ItemMeta(['key' => $meta->key, 'value' => $meta->value]);
                    $model->metas()->save($metaModel);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        Storage::delete("/public/items/" . $item->image_path);
        $item->items()->detach();
        return $item->delete();
    }

    public function getItems(Request $request)
    {
        $item = new Item;

        if ($request->query('search')) {
            $search = $request->query('search');
            $item = $item->where(function ($q) use($search) {
                $q->where('name', 'like', '%' . $search . '%')
                    ->orWhere('description', 'like', '%' . $search . '%');
            });
        }

        if ($request->query('tag')) {
            $tagName = $request->query('tag');
            $item = $item->whereHas('tags', function ($query) use ($tagName) {
                $query->where('name', $tagName);
            });
        }

        if ($request->query('limit')) {
            $item = $item->take($request->query('limit'));
        }

        return $item->orderBy('updated_at', 'desc')->get();
    }

    public function getItem($id)
    {
        return Item::findOrFail($id);
    }
}
