<?php

namespace App\Http\Controllers;

use App\ItemMeta;
use Illuminate\Http\Request;

class ItemMetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ItemMeta  $itemMeta
     * @return \Illuminate\Http\Response
     */
    public function show(ItemMeta $itemMeta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ItemMeta  $itemMeta
     * @return \Illuminate\Http\Response
     */
    public function edit(ItemMeta $itemMeta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ItemMeta  $itemMeta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemMeta $itemMeta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ItemMeta  $itemMeta
     * @return \Illuminate\Http\Response
     */
    public function destroy(ItemMeta $itemMeta)
    {
        //
    }
}
