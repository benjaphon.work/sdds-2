<?php

namespace App\Http\Controllers;

use App\Leave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->only(['approval']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'description' => 'required|max:100',
            'image' => 'nullable|sometimes|image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);

        $model = new Leave;

        $model->description = $request->description;
        $model->start_at = $request->start_at;
        $model->end_at = $request->end_at;
        $model->type = $request->type;
        $model->status = ($request->type == 1) ? 0 : 1;
        $model->user_id = $request->user_id;
        $model->created_id = auth()->user()->id;

        if ($request->file('image')) {
            $model->image_path = basename($request->file('image')->store('leaves', 'public'));
        }

        $model->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function show(Leave $leave)
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function edit(Leave $leave)
    {
        return view('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Leave $leave)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Leave  $leave
     * @return \Illuminate\Http\Response
     */
    public function destroy(Leave $leave)
    {
        Storage::delete("/public/leaves/" . $leave->image_path);
        return $leave->delete();
    }

    public function getLeave($id)
    {
        return Leave::findOrFail($id);
    }

    public function getLeaves(Request $request)
    {
        $leave = new Leave;

        if ($request->query('search')) {
            $search = $request->query('search');
            $leave = $leave->where(function ($q) use ($search) {
                $q->where('description', 'like', '%' . $search . '%');
            });
        }

        if ($request->query('limit')) {
            $leave = $leave->take($request->query('limit'));
        }

        return $leave->orderBy('updated_at', 'desc')->get();
    }

    public function approval($id, Request $request)
    {
        $model = Leave::findOrFail($id);
        $model->status = $request->query('command');
        $model->save();
    }
}
