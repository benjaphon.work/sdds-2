<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Project::create($request->all());

        $validated = $request->validate([
            'name' => 'required|unique:projects,name|max:255',
            'location' => 'required',
        ]);

        $project = new Project;

        $project->name = $request->name;
        $project->location = $request->location;
        $project->installed_at = $request->installed_at;
        $project->dismantle_at = $request->dismantle_at;
        $project->status = $request->status;
        $project->owner_id = isset($request->owner["id"]) ? $request->owner["id"] : null;

        if ($project->save()) {

            $project->employees()->detach();
            foreach ($request->employees as $key => $value) {
                if (isset($value['id'])) {
                    $project->employees()->attach($value['id']);
                }
            }

            $project->tasks()->delete();
            foreach ($request->tasks as $key => $value) {
                $task = new Task([
                    'name' => $value['name'],
                    'completed' => $value['completed'],
                ]);
                $project->tasks()->save($task);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $validated = $request->validate([
            'name' => "required|unique:projects,name,{$project->id}|max:255",
            'location' => 'required',
        ]);

        $project->name = $request->name;
        $project->location = $request->location;
        $project->installed_at = $request->installed_at;
        $project->dismantle_at = $request->dismantle_at;
        $project->status = $request->status;
        $project->owner_id = isset($request->owner["id"]) ? $request->owner["id"] : null;

        // dd($request);

        if ($project->save()) {

            $project->employees()->detach();
            foreach ($request->employees as $key => $value) {
                if (isset($value['id'])) {
                    $project->employees()->attach($value['id']);
                }
            }

            $project->tasks()->delete();
            foreach ($request->tasks as $key => $value) {
                $task = new Task([
                    'name' => $value['name'],
                    'completed' => $value['completed'],
                ]);
                $project->tasks()->save($task);
            }

        }

        $project->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

    public function getProjects()
    {
        return Project::orderBy('updated_at', 'desc')->get();
    }

    public function followProject($projectId)
    {
        $project = Project::findOrFail($projectId);

        $project->employees()->attach(Auth::id());

        return Auth::id();
    }

    public function unFollowProject($projectId)
    {
        $project = Project::findOrFail($projectId);

        $project->employees()->detach(Auth::id());

        return Auth::id();
    }

    public function search(Request $request)
    {
        return Project::where('name', 'like', '%' . $request->search . '%')
            ->orWhere('location', 'like', '%' . $request->search . '%')
            ->orderBy('updated_at', 'desc')->get();
    }

    public function getProject($id)
    {
        return Project::findOrFail($id);
    }
}
