<?php

namespace App\Http\Controllers;

use App\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        return Task::latest()->get();
    }
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:500',
        ]);
        return Task::create(['name' => request('name')]);
    }
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return 204;
    }
}
