<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin')->only(['create', 'store', 'edit', 'update', 'approval']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts.admin');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'email' => 'required|unique:users,email|max:100',
            'image' => 'nullable|sometimes|image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);

        $model = new User;

        $model->name = $request->first_name . ' ' . $request->last_name;
        $model->first_name = $request->first_name;
        $model->last_name = $request->last_name;
        $model->nick_name = $request->nick_name;
        $model->email = $request->email;
        $model->department_id = $request->department_id;
        $model->role = $request->role;
        $model->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        $model->approved_at = now();

        if ($request->file('image')) {
            $model->avatar = basename($request->file('image')->store('users', 'public'));
        }

        $model->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('layouts.admin');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('layouts.admin');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $model = $user;

        $validated = $request->validate([
            'image' => 'nullable|sometimes|image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);

        $model->name = $request->first_name . ' ' . $request->last_name;
        $model->first_name = $request->first_name;
        $model->last_name = $request->last_name;
        $model->nick_name = $request->nick_name;
        $model->department_id = $request->department_id;
        $model->role = $request->role;

        if ($request->file('image')) {
            Storage::delete("/public/users/" . $model->avatar);
            $model->avatar = basename($request->file('image')->store('users', 'public'));
        }

        if ($request['reset_image'] === 'true') {
            Storage::delete("/public/users/" . $model->avatar);
            $model->avatar = null;
        }

        $model->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getProjectsById($id)
    {
        $ownProjects = User::findOrFail($id)->own_projects()->get();
        $projects = User::findOrFail($id)->projects()->get();
        return $ownProjects->merge($projects);
    }

    public function getUser($id)
    {
        return User::findOrFail($id);
    }

    public function getUsers(Request $request)
    {
        $user = new User;

        if ($request->query('search')) {
            $search = $request->query('search');
            $user = $user->where('first_name', 'like', '%' . $search . '%')
                    ->orWhere('last_name', 'like', '%' . $search . '%')
                    ->orWhere('nick_name', 'like', '%' . $search . '%')
                    ->orWhere('email', 'like', '%' . $search . '%')
                    ->orWhereHas('department', function($q) use ($search) {
                        $q->where('name', 'like', '%' . $search . '%');
                    });
        }

        if ($request->query('limit')) {
            $user = $user->take($request->query('limit'));
        }

        return $user->orderBy('updated_at', 'desc')->get();
    }

    public function approve($id, Request $request)
    {
        $model = User::findOrFail($id);
        $model->approved_at = now();
        $model->save();
    }
}
