<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Error;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!auth()->check() || auth()->user()->role != 'admin') {
            return response('Permission Denied', 200);
        }

        return $next($request);
    }
}
