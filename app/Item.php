<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $appends = ['items', 'metas', 'tags'];

    public function projects()
    {
        return $this->belongsToMany(Project::class); 
    }
    
    public function items()
    {
        return $this->belongsToMany(Item::class, 'items_items', 'item_id', 'related_id');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'items_tags', 'item_id', 'tag_name', 'id', 'name');
    }

    public function metas()
    {
        return $this->hasMany(ItemMeta::class);
    }

    public function getItemsAttribute()
    {
        return $this->items()->get();
    }

    public function getMetasAttribute()
    {
        return $this->metas()->get();
    }

    public function getTagsAttribute()
    {
        return $this->tags()->get();
    }
}
