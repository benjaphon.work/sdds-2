<?php

namespace App;

use App\Classifier;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Leave extends Model
{
    use HasFactory;

    protected $appends = ['user', 'classifier_type', 'classifier_status'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    public function getClassifierTypeAttribute()
    {
        return Classifier::where('table', 'leaves')->where('field', 'type')->where('value', $this->type)->first();
    }

    public function getClassifierStatusAttribute()
    {
        return Classifier::where('table', 'leaves')->where('field', 'status')->where('value', $this->status)->first();
    }
}
