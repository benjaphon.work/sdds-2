<?php

namespace App;

use App\Task;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    use HasFactory;

    protected $appends = ['owner', 'percentage', 'employees', 'tasks', 'show_url', 'edit_url'];
    
    public function tasks()
    {
        return $this->hasMany('App\Task', 'project_id', 'id');
    }

    public function employees()
    {
        return $this->belongsToMany('App\User', 'projects_users', 'project_id', 'user_id');
    }

    public function items()
    {
        return $this->belongsTo(Item::class);
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id', 'id');
    }

    public function getOwnerAttribute()
    {
        return $this->owner()->first();
    }

    public function getPercentageAttribute()
    {
        $tasks = $this->tasks()->count();
        $taskCompleted = $this->tasks()->where('completed', 1)->get()->count();
        return $tasks ? ( ($taskCompleted * 100) / $tasks ) : 0;
    }

    public function getEmployeesAttribute()
    {
        return $this->employees()->get();
    }

    public function getTasksAttribute()
    {
        return $this->tasks()->get();
    }

    public function getShowUrlAttribute()
    {
        return route('project.show', $this->id); 
    }

    public function getEditUrlAttribute()
    {
        return route('project.edit', $this->id); 
    }
}
