<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    public $timestamps = false;
    public $incrementing = false;

    protected $primaryKey = 'name';

    protected $fillable = [
        'name',
    ];

    public function items()
    {
        return $this->belongsToMany('App\Item', 'items_tags', 'tag_name', 'item_id', 'name', 'id');
    }
}
