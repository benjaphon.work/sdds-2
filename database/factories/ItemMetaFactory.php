<?php

namespace Database\Factories;

use App\ItemMeta;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemMetaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ItemMeta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
