<?php

namespace Database\Factories;

use App\Leave;
use Illuminate\Database\Eloquent\Factories\Factory;

class LeaveFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Leave::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'description' => $this->faker->text(20),
            'start_at' => $this->faker->dateTimeThisMonth(),
            'end_at' => $this->faker->dateTimeThisMonth(),
            'type' => $this->faker->numberBetween(1, 2),
            'status' => $this->faker->numberBetween(0, 2),
            'user_id' => $this->faker->randomDigit,
            'created_id' => $this->faker->randomDigit,
        ];
    }
}
