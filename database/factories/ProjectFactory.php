<?php

namespace Database\Factories;

use App\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company,
            'location' => $this->faker->address,
            'installed_at' => $this->faker->dateTimeThisMonth(),
            'dismantle_at' => $this->faker->dateTimeThisMonth(),
            'status' => $this->faker->randomElement(["Planning", "Designing", "Purchasing", "Production", "Installing", "Demantle", "Finished"]),
            'owner_id' => $this->faker->randomDigit,
        ];
    }
}
