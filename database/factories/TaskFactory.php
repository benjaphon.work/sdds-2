<?php

namespace Database\Factories;

use App\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement(["Planning", "Designing", "Purchasing", "Production", "Installing", "Demantle", "Finished"]),
            'completed' => $this->faker->boolean,
            'project_id' => $this->faker->randomDigit,
        ];
    }
}
