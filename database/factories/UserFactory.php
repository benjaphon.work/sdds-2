<?php

namespace Database\Factories;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName,
            'nick_name' => $this->faker->firstName(),
            'email' => $this->faker->unique()->safeEmail,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'department_id' => $this->faker->numberBetween(1,5),
            'remember_token' => Str::random(10),
            'approved_at' => now(),
        ];
    }
}
