<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->id();
            $table->string('description')->nullable();
            $table->date('start_at');
            $table->date('end_at');
            $table->integer('type')->unsigned();
            $table->integer('status')->unsigned();
            $table->string('image_path')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('created_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
