<?php

namespace Database\Seeders;

use App\Classifier;
use Illuminate\Database\Seeder;

class ClassifierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classifier::truncate();

        $leaves = [
            [
                'table' => 'leaves',
                'field' => 'type',
                'name' => 'Business',
                'value' => 1,
            ],
            [
                'table' => 'leaves',
                'field' => 'type',
                'name' => 'Sick',
                'value' => 2,
            ],
            [
                'table' => 'leaves',
                'field' => 'status',
                'name' => 'waiting',
                'value' => 0,
            ],
            [
                'table' => 'leaves',
                'field' => 'status',
                'name' => 'approved',
                'value' => 1,
            ],
            [
                'table' => 'leaves',
                'field' => 'status',
                'name' => 'cancelled',
                'value' => 2,
            ],
        ];

        Classifier::insert($leaves);
    }
}
