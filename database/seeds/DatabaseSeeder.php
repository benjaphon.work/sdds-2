<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call([
            UserSeeder::class,
            ProjectSeeder::class,
            TaskSeeder::class,
            DepartmentSeeder::class,
            ItemSeeder::class,
            LeaveSeeder::class,
            ClassifierSeeder::class,
        ]);

        DB::table('projects_users')->truncate();
        DB::table('items_items')->truncate();
        DB::table('tags')->truncate();
        DB::table('social_accounts')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
