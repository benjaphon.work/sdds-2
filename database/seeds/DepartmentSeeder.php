<?php

namespace Database\Seeders;

use App\Department;
use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::truncate();

        $departments = [
            ['name' => 'AE'],
            ['name' => 'Design'],
            ['name' => 'Operation'],
            ['name' => 'Factory'],
            ['name' => 'Freelance'],
        ];

        Department::insert($departments);
    }
}
