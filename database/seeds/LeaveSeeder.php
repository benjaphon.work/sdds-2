<?php

namespace Database\Seeders;

use App\Leave;
use Illuminate\Database\Seeder;

class LeaveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Leave::truncate();

        Leave::factory()->count(10)->create();
    }
}
