<?php

namespace Database\Seeders;

use App\Project;
use App\User;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Project::truncate();

        Project::factory()->count(5)->create();
    }
}
