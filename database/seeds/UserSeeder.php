<?php

namespace Database\Seeders;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::insert([
            'name' => 'Admin Admin',
            'first_name' => 'Admin',
            'last_name' => 'Admin',
            'nick_name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'role' => 'admin',
            'remember_token' => Str::random(10),
            'department_id' => 1,
            'approved_at' => now(),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        // User::insert([
        //     'name' => 'Benjaphon Niramit',
        //     'first_name' => 'Benjaphon',
        //     'last_name' => 'Niramit',
        //     'nick_name' => 'Ben',
        //     'email' => 'benjaphon.mail@gmail.com',
        //     'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        //     'role' => 'admin',
        //     'remember_token' => Str::random(10),
        //     'department_id' => 1,
        //     'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // User::insert([
        //     'name' => 'Sirima Whangnok',
        //     'first_name' => 'Sirima',
        //     'last_name' => 'Whangnok',
        //     'nick_name' => 'Biew',
        //     'email' => 'sirimawhangnok@gmail.com',
        //     'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        //     'role' => 'user',
        //     'remember_token' => Str::random(10),
        //     'department_id' => 2,
        //     'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        User::factory()->count(4)->hasProjects(3)->create();
    }
}
