require('./bootstrap');

$('[own-data-toggle=dropdown]').on('click', function (event) {
    $(this).parent().toggleClass('show');
    $(this).parent().find('.dropdown-menu').addClass('show');
});

$('body').on('click', function (e) {
    if (!$('[own-data-toggle=dropdown]').is(e.target) &&
        $('[own-data-toggle=dropdown]').has(e.target).length === 0 &&
        $('.show').has(e.target).length === 0
    ) {
        $('[own-data-toggle=dropdown]').parent().find('.dropdown-menu').removeClass('show');
    }
});

window.setTimeout(function () {
    $(".alert").fadeTo(500, 0).slideUp(500, function () {
        $(this).remove();
    });
}, 5000);
