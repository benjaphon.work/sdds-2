import Home from './views/Home.vue';
import Example from './views/Example.vue';
import CalendarIndex from './views/calendar/CalendarIndexComponent.vue'
import ProjectIndex from './views/project/ProjectIndexComponent.vue'
import ProjectForm from './components/project/ProjectFormComponent.vue'
import ItemIndex from './views/item/ItemIndexComponent.vue'
import ItemForm from './components/item/ItemFormComponent.vue'
import ItemCard from './components/item/ItemCardComponent.vue'
import LeaveIndex from './views/leave/LeaveIndexComponent.vue'
import LeaveShow from './views/leave/LeaveShowComponent.vue'
import LeaveCreate from './views/leave/LeaveCreateComponent.vue'
import UserIndex from './views/user/UserIndexComponent.vue'
import UserShow from './views/user/UserShowComponent.vue'
import UserCreate from './views/user/UserCreateComponent.vue'
import UserEdit from './views/user/UserEditComponent.vue'
export const routes = [
    {
        path: '/',
        component: Home,
        name: 'HomeBlank'
    },
    {
        path: '/home',
        component: Home,
        name: 'Home'
    },
    {
        path: '/vue/example',
        component: Example,
        name: 'Example'
    },
    {
        path: '/calendar',
        component: CalendarIndex,
        name: 'CalendarIndex'
    },
    {
        path: '/project',
        component: ProjectIndex,
        name: 'ProjectIndex'
    },
    {
        path: '/project/create',
        component: ProjectForm,
        name: 'ProjectCreate'
    },
    {
        path: '/project/:id',
        component: ProjectForm,
        name: 'ProjectShow'
    },
    {
        path: '/project/:id/edit',
        component: ProjectForm,
        name: 'ProjectEdit'
    },
    {
        path: '/item',
        component: ItemIndex,
        name: 'ItemIndex'
    },
    {
        path: '/item/tag/:tag',
        component: ItemIndex,
        name: 'ItemTag'
    },
    {
        path: '/item/create',
        component: ItemForm,
        name: 'ItemCreate'
    },
    {
        path: '/item/:id',
        component: ItemCard,
        name: 'ItemShow'
    },
    {
        path: '/item/:id/edit',
        component: ItemForm,
        name: 'ItemEdit'
    },
    {
        path: '/leave',
        component: LeaveIndex,
        name: 'LeaveIndex'
    },
    {
        path: '/leave/create',
        component: LeaveCreate,
        name: 'LeaveCreate'
    },
    {
        path: '/leave/:id',
        component: LeaveShow,
        name: 'LeaveShow'
    },
    {
        path: '/user',
        component: UserIndex,
        name: 'UserIndex'
    },
    {
        path: '/user/create',
        component: UserCreate,
        name: 'UserCreate'
    },
    {
        path: '/user/:id/edit',
        component: UserEdit,
        name: 'UserEdit'
    },
    {
        path: '/user/:id',
        component: UserShow,
        name: 'UserShow'
    },
];
