import Vue from 'vue'
import VueRouter from 'vue-router';
import {
    routes
} from './routes';
import Vuetify from 'vuetify';
import VueCurrencyFilter from 'vue-currency-filter'

import BCard from './components/CardComponent';
import ProjectForm from './components/project/ProjectFormComponent';
import UserForm from './components/UserFormComponent';
import CalendarSearch from "./components/CalendarSearchComponent.vue";
import ProjectIndex from './views/project/ProjectIndexComponent.vue'
import ItemIndex from './views/item/ItemIndexComponent.vue';

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueCurrencyFilter, [{
    symbol: '฿',
    thousandsSeparator: ',',
    fractionCount: 0,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true,
    avoidEmptyDecimals: undefined,
}, {
    name: 'number',
    symbol: '',
    thousandsSeparator: ',',
    fractionCount: 0,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true,
    avoidEmptyDecimals: undefined,
}]);

if (document.querySelector("meta[name='user-id']")) {
    Vue.prototype.$userId = document.querySelector("meta[name='user-id']").getAttribute('content');
}

if (document.querySelector("meta[name='is-admin']")) {
    Vue.prototype.$isAdmin = document.querySelector("meta[name='is-admin']").getAttribute('content');
}

const router = new VueRouter({
    mode: 'history',
    routes
});

window.vm = new Vue({
    el: '#wrapper',
    router,
    vuetify: new Vuetify(),
    components: {
        BCard,
        ProjectForm,
        UserForm,
        CalendarSearch,
        ProjectIndex,
        ItemIndex,
    },
    watch: {
        '$route'() {
            $('.collapse').collapse('hide');
        }
    }
});
