@extends('layouts.admin')

@section('main-content')

<user-form   title="User Create"
                action="create"
                back-url="{{ route('user.index') }}"
                save-url="{{ route('user.store') }}">
</user-form>

@endsection