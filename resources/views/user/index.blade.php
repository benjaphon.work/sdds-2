@extends('layouts.admin')

@section('main-content')


<div class="container">
    
    <b-card>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('First Name') }}</th>
                        <th>{{ __('Last Name') }}</th>
                        <th>{{ __('Nick Name') }}</th>
                        <th>{{ __('Email') }}</th>
                        <th>{{ __('Department') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($users as $user)
                    <tr>
                        <td scope="row"><a href="{{ route('user.create') }}">{{ $loop->iteration }}</a></td>
                        <td>{{ $user->first_name }}</td>
                        <td>{{ $user->last_name }}</td>
                        <td>{{ $user->nick_name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->department->name }}</td>
                    </tr>
                    @empty
                        <tr>
                            <td class="text-center" colspan="6">{{ __('There is no user record') }}</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </b-card>
</div>

@endsection