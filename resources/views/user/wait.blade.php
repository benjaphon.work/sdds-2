@extends('layouts.auth')

@section('main-content')
<div class="container">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <div class="row">
                <div class="col-12">

                    <div class="text-center p-5">
                        <h2 class="text-gray-800 mb-5">Waiting For Approval</h2>
                        <h4 class="text-gray-600 mb-0">You have to wait until admin allow you to use the website.</h3>
                            <a href="/home">← Back to Dashboard</a>
                            |
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout
                                →</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection