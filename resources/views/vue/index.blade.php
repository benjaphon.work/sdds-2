@extends('layouts.admin')

@section('main-content')
<h1>This is a vue component</h1>
<router-link to="/vue">Back to the root</router-link>
<router-view></router-view>
@endsection