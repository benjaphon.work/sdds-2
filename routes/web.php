<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/vue/{any?}', function () {
    return view('vue.index');
})->where('any', '[\/\w\.-]*');

Route::get('register/request', 'Auth\RegisterController@requestInvitation')->name('requestInvitation');
Route::post('invitations', 'InvitationsController@store')->middleware('guest')->name('storeInvitation');

Route::get('item/tag/{tag}', 'ItemController@index')->name('item.tag');

Route::group([
    'middleware' => ['auth', 'admin'],
    'prefix' => 'invitations',
], function () {
    Route::get('/', 'InvitationsController@index')->name('showInvitations');
});

Route::get('user/wait', function () {
    return view('user.wait');
})->name('user.wait');

Route::group(['middleware' => ['auth', 'approval']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/calendar', function () {
        return view('layouts.admin');
    })->name('calendar.index');

    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::put('/profile', 'ProfileController@update')->name('profile.update');
    
    Route::resource('project', 'ProjectController');
    Route::resource('user', 'UserController');
    Route::resource('item', 'ItemController');
    Route::resource('leave', 'LeaveController'); 

    Route::get('/', 'HomeController@index');

});

Route::group(['prefix' => 'api', 'middleware' => ['auth']], function () {
    Route::resource('tasks', 'TaskController');
    Route::get('/user/{id}/projects', 'UserController@getProjectsById')->name('user.projects');
    Route::get('/department/{id}/projects', 'DepartmentController@getProjectsById')->name('department.projects');
    Route::get('/user/all', 'UserController@getUsers')->name('user.all');
    Route::get('/user/get/{id}', 'UserController@getUser')->name('user.get');
    Route::get('/project/all', 'ProjectController@getProjects')->name('project.all');
    Route::get('/project/get/{id}', 'ProjectController@getProject')->name('project.get');
    Route::post('/project/search', 'ProjectController@search')->name('project.search');
    Route::get('/department/all', 'DepartmentController@getDepartments')->name('department.all');
    Route::get('/project/{id}/follow', 'ProjectController@followProject')->name('project.follow');
    Route::get('/project/{id}/unfollow', 'ProjectController@unFollowProject')->name('project.unfollow');
    Route::get('/item/all', 'ItemController@getItems')->name('item.all');
    Route::get('/item/get/{id}', 'ItemController@getItem')->name('item.get');
    Route::get('/tag/all', 'TagController@getTags')->name('tag.all');
    Route::get('/leave/get/{id}', 'LeaveController@getLeave')->name('leave.get');
    Route::get('/leave/all', 'LeaveController@getLeaves')->name('leave.all');
    Route::get('/leave/{id}/approval', 'LeaveController@approval')->name('leave.approval');
    Route::get('/user/{id}/approve', 'UserController@approve')->name('user.approve');

    Route::get('/classifier/list/{table}/{field}', 'ClassifierController@list')->name('classifier.list');
});

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
})->name('locale');

Auth::routes();

Route::get('register', 'Auth\RegisterController@showRegistrationForm')
    ->name('register')
    ->middleware('hasInvitation');

Route::prefix('login')->group(function () {
    Route::get('/{provider}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
    Route::get('/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.provider.callback');
});